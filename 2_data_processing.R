# this script reads in the individual csvs for both hand-tagged
# and crossref data, and merges everything into one big csv

library(readr)
library(stringi)

read <- function(filename) {
  data <- read_csv(paste(readfrom, filename, sep=""))
  return(data)
}

readfrom <- "hand-tagged/"

hand_tagged <- rbind(
  #read("pubdata - TACL.csv"),       # removed because we decided not to include comp ling
  read("pubdata - Frontiers.csv"),
  read("pubdata - Acquisition.csv"),
  read("pubdata - BrainLang.csv"),
  read("pubdata - NLLT.csv"),
  read("pubdata - SemPrag.csv"),
  read("pubdata - Phonetics.csv"),
  #read("pubdata - JML.csv"),       # removed b/c of errors, not useful hand-tagging
  read("pubdata - SALT.csv"),
  read("pubdata - BUCLD.csv"),
  read("pubdata - LI.csv"),
  read("pubdata - PLC.csv"),
  read("pubdata - Glossa.csv"),
  read("pubdata - AMP.csv"),
  read("pubdata - Phonology.csv"),
  read("pubdata - Morphology.csv"),
  read("pubdata - NELS.csv"),
  read("pubdata - Lingua.csv"),
  read("pubdata - WCCFL.csv"),
  read("pubdata - LLD.csv"))

readfrom <- "crossref/"

crossref <- rbind(
  read("annual_meeting_on_phonology.csv"),
  read("brain_and_language.csv"),
  read("glossa.csv"),
  read("journal_of_memory_and_language.csv"),
  read("journal_of_phonetics.csv"),
  read("language_acquisition.csv"),
  read("language_learning_and_development.csv"),
  read("lingua.csv"),
  read("linguistic_inquiry.csv"),
  read("morphology.csv"),
  read("natural_language_and_linguistic_theory.csv"),
  read("phonology.csv"),
  read("semantics_and_linguistic_theory.csv"),
  read("semantics_and_pragmatics.csv"),
  read("syntax.csv"),
  read("frontiers_in_psychology.csv"),
  read("american_speech.csv"),
  read("cognitive_linguistics.csv"),
  read("journal_of_language_contact.csv"),
  read("journal_of_speech_language_and_hearing_research.csv"),
  read("laboratory_phonology.csv"),
  read("language.csv"),
  read("language_and_linguistics_compass.csv"),
  read("language_and_speech.csv"),
  read("stuf_-_language_typology_and_universals.csv"),
  read("linguistic_variation.csv"),
  read("acoustical_society.csv"))

# Remove unnecessary columns 
crossref$subfield <- NULL
crossref$method <- NULL
crossref$type <- NULL

# Rename hand-tagged data columns so they all match crossref and have no spaces
colnames(hand_tagged) <- c("journal_name", "year", "volume", "issue", "author_name",
                    "num_authors", "auth_num", "title","subfield","type","method",
                    "coder","subfield_tagging","type_tagging","method_tagging")

# Replace non-ascii characters and make everything lower case
fix_text <- function(dataframe) {
  dataframe$title <- tolower(dataframe$title)
  dataframe$title <- stri_trans_general(dataframe$title,"latin-ascii")
  dataframe$author_name <- tolower(dataframe$author_name)
  dataframe$author_name <- stri_trans_general(dataframe$author_name,"latin-ascii")
  dataframe$id <- paste(dataframe$title,dataframe$author_name,sep=" auth: ")
  return(dataframe)
}

# merge them
full_data <- merge(fix_text(crossref), fix_text(hand_tagged), by="id", all = TRUE)

# in cases where there's data from both the hand-tagged set and the crossref set,
# use the hand tagged data (more likely to be accurate)
prefer_hand <- function(data, col) {
  col_keep <- paste(col,".x",sep="")
  other_col <- paste(col,".y",sep="")
  data[,col_keep] <- ifelse(is.na(data[,col_keep]),data[,other_col],data[,col_keep])
  names(data)[names(data) == col_keep] <- col
  return(data[ , !names(data) %in% c(other_col)])
}

for(column in c("journal_name","year","volume",
                "issue","author_name","num_authors",
                "auth_num","title")) {
  full_data <- prefer_hand(full_data,column)
}

# unique(full_data$journal_names) shows that there are some mis-named journals
# so we'll just manually fix those
full_data$journal_name <- ifelse(grepl("Frontier",full_data$journal_name),"Frontiers in Psychology",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Laboratory Phonology",full_data$journal_name),"Laboratory Phonology",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Linguistic Variation",full_data$journal_name),"Linguistic Variation",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Natural Language & Linguistic Theory",full_data$journal_name),"Natural Language and Linguistic Theory",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Language Acquisition",full_data$journal_name),"Language Acquisition",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Acoustical Society of America",full_data$journal_name),"Journal of the Acoustical Society of America",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Glossa",full_data$journal_name),"Glossa: a journal of general linguistics",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Annual Meetings on Phonology",full_data$journal_name),"Proceedings of the Annual Meetings on Phonology",full_data$journal_name)
full_data$journal_name <- ifelse(grepl("Semantics and Linguistic Theory",full_data$journal_name),"Semantics and Linguistic Theory",full_data$journal_name)

# We want to filter the Acoustical Society and Frontiers to only include articles where
# at least one co-author is a linguist. To do that we'll need to do some name matching.
# Here's a function to remove the middle initial (if there is one) for all author names.
remove_mi <- function(data) {
  data <- separate(data = data, col = author_name, into = c("first", "second","third","fourth","fifth"), sep = "\\ ")
  data$second <- ifelse(grepl("\\.",data$second) | grepl("\\(",data$second) | nchar(data$second) == 1, NA, data$second)
  data$author_name <- ifelse(is.na(data$second),data$third,
                             ifelse(is.na(data$third),data$second,
                                    paste(data$second,data$third,sep=" ")))
  data$author_name <- ifelse(is.na(data$fourth),data$author_name,paste(data$author_name,data$fourth,sep=" "))
  data$author_name <- ifelse(is.na(data$fifth),data$author_name,paste(data$author_name,data$fifth,sep=" "))
  data$author_name <- paste(data$first,data$author_name,sep=" ")
  data <- subset(data,select = -c(first,second,third,fourth,fifth))
  return(data)
}

full_data <- remove_mi(full_data)

filter <- full_data[full_data$journal_name == "Frontiers in Psychology" |
                      full_data$journal_name == "Journal of the Acoustical Society of America",]
core <- full_data[full_data$journal_name != "Frontiers in Psychology" &
                    full_data$journal_name != "Journal of the Acoustical Society of America",]

# The intersection of unique authors from each list gives us the list of authors from the non-ling journals
# who have also published in a ling journal. We're creating a list of article titles where at least one
# of the coauthors of the article is in that intersected list.
keep_articles <- as.list(unique(filter[filter$author_name %in% as.list(intersect(unique(core$author_name), unique(filter$author_name))),"title"]))

# Merge with the core set
d <- rbind(filter[filter$title %in% keep_articles,],core)
rm(core, filter)

# Now add gender. For now, I'm doing this with the "gender" package, ssa method.
# In the actual analysis we used "genderize", but that has a fee.

# The genderize version is there too, but commented out.
# In the r script 0_verify_gender_tagging you can see our comparisons of various
# gender tagging approaches on a hand-tagged set of 810 linguists.
library(gender)
library(genderdata)
readfrom <- ""

# pull out first name and add it
d$first <- separate(data = d, col = author_name, into = c("first", "second"), sep = "\\ ")$first

# If only one character was found as first name (or one char and a dot), remove it
d <- d[!(nchar(d$first) <= 1) & !(nchar(d$first == 2) & substr(d$first,2,2) == "."),]

names <- unique(d$first)

# Automatically tag gender with ssa method
guesses <- gender(names, years = c(1900, 2012), method = "ssa")
guesses <- unique(guesses[,c(1,4)])
#write.csv(guesses, file = "gender_data.csv")

# or with genderize method, which is able to do more
#guesses <- genderizeAPI(names, apikey=NULL)
#guesses <- unique(guesses[,c(1,2)])
#guesses <- na.omit(guesses)
#write.csv(guesses, file = "gender_data.csv")

# Once we have a csv of first names and genders, we need to match them up with the authors.
# So we'll just open the list of names that we got from genderize when we paid for it.
guesses <- read("gender_data.csv")
guesses <- guesses[,2:3]

# From here down works just the same whether you're using the list of guessed genders
# from the gender package or you read in the csv from the genderize package
colnames(guesses) <- c("first","gender")
d <- merge(d, guesses, by="first")

# Now add subfield info. We generated this csv using the hand-tagged dataset in 0_find_subfield.R
# But you could also just write a could just as well simply write a csv of journal names and subfields.
subfields <- read("subfields.csv")
d <- merge(d, subfields, all.x = TRUE)
rm(subfields)

# Now add reviewing info. We looked this info up on journals' websites.
reviewing <- read("reviewing.csv")
d <- merge(d, reviewing, all.x = TRUE)
rm(reviewing)

write.csv(d, file = "tagged_pub_data.csv", row.names = FALSE)
write.csv(full_data, file = "raw_pub_data.csv", row.names = FALSE)


